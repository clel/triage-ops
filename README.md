# GitLab triage operations

This is a prototype. The goal is to add immediate value to the GitLab Engineering function, while determining if it adds value to customers. If so, we will work with Victor Wu in Product Management to bring this functionality to GitLab the product.

## The schedules

### Triage operations schedules (daily)

* [CE daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/10512/edit)
* [EE daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/10515/edit)
* [gitlab-org group daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/11219/edit)
* [charts group daily triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/19768/edit)
* [www-gitlab-com daily schedule](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/18297/edit)

### Triage operations schedules (weekly)

* [CE weekly triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/11986/edit)
* [EE weekly triage](https://gitlab.com/gitlab-org/quality/triage-ops/pipeline_schedules/11987/edit)

## The bot

We're using [@gitlab-bot](https://gitlab.com/gitlab-bot) as the user to run
triage operations. The credentials could be found in the shared 1Password
vault.
