export TRIAGE_SOURCE_PATH=${TRIAGE_SOURCE_PATH:-gitlab-org/gitlab-ce}
export TRIAGE_SOURCE_TYPE=${TRIAGE_SOURCE_TYPE:-projects}
export GITLAB_TRIAGE_FILE=${GITLAB_TRIAGE_FILE:-.triage-policies.yml}
export GITLAB_TRIAGE_BRANCH=${GITLAB_TRIAGE_BRANCH:-master}

env | grep -E \
'TRIAGE_SOURCE_PATH|TRIAGE_SOURCE_TYPE|GITLAB_TRIAGE_FILE|GITLAB_TRIAGE_BRANCH'
